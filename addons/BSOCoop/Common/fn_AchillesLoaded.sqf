#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:
    checks if achilles is loaded on the client

    Parameter(s):
    None

    Returns:
    bool if Achilles is Loaded
*/

(isClass (configFile >> "CfgPatches" >> "achilles_functions_f_achilles"))
 && (isClass (configFile >> "CfgPatches" >> "achilles_functions_f_ares"))
