#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:
    initilize Common Module

    Parameter(s):
    None

    Returns:
    None
*/
if !(isClass (configFile >> "CfgPatches" >> "ace_arsenal")) exitWith {};
// Port Loadouts from Vanilla Arsenal to ACE Arsenal
DFUNC(portLoadouts) = {
    params [["_override", false, [false]]];
    private _VALoadouts = +(profilenamespace getvariable ["bis_fnc_saveInventory_data",[]]);
    private _aceLoadouts = +(profileNamespace getVariable ["ace_arsenal_saved_loadouts",[]]);

    if (_VALoadouts isEqualTo []) exitWith {};

    for "_i" from 0 to (count _VALoadouts - 1) step 2 do {

       private _name = _VALoadouts select _i;
       hintSilent ("Port Over: " + str _name);
       private _sameNameLoadoutsList = _aceLoadouts select {_x select 0 == _name};
       [player, [profilenamespace, _name]] call bis_fnc_loadinventory;
       private _loadout = getUnitLoadout player;

       if (count _sameNameLoadoutsList > 0) then {
           if (_override) then {
               _aceLoadouts set [_aceLoadouts find (_sameNameLoadoutsList select 0), [_name, _loadout]];
           };
       } else {
           _aceLoadouts pushBack [_name, _loadout];
       };
       sleep 0.5;
    };
    profileNamespace setVariable ["ace_arsenal_saved_loadouts", _aceLoadouts];
    hintSilent "Port Done";
};

DFUNC(callPortLoadouts) = {
    if !(call FUNC(isNearBase)) exitWith {};
    [
        "Since ACE 3.12.0 there is a New Arsenal. Do you want to Port your old Loadouts to the new system?",
        "Port ACE Arsenal?",
        ["Yes", {
            closeDialog 1;
            [{
                [
                    "Overwrite Existing Loadouts",
                    "Overwrite?",
                    ["Yes", {
                        _this spawn {
                            private _loadout = getUnitLoadout CLib_Player;
                            closeDialog 1;
                            disableUserInput true;
                            true call FUNC(portLoadouts);
                            CLib_Player setUnitLoadout [_loadout, false];
                            profileNamespace setVariable [QGVAR(portOverLoadouts), false];
                            disableUserInput false;
                        };
                    }],
                    ["No", {
                        _this spawn {
                            private _loadout = getUnitLoadout CLib_Player;
                            closeDialog 1;
                            disableUserInput true;
                            false call FUNC(portLoadouts);
                            CLib_Player setUnitLoadout [_loadout, false];
                            profileNamespace setVariable [QGVAR(portOverLoadouts), false];
                            disableUserInput false;
                        };
                    }],
                    {},
                    []
                ] call CFUNC(messageBox);
            }] call CFUNC(execNextFrame);
        }],
        ["No", { closeDialog 1; profileNamespace setVariable [QGVAR(portOverLoadouts), false]; }],
        {},
        []
    ] call CFUNC(messageBox);
};

[
    "BSO Coop",
    "Port_ACE_Loadouts",
    "Port ACE Loadouts",
    {
        call FUNC(callPortLoadouts);
    }, "", [DIK_F7, [false, false, false]]
] call CBA_fnc_addKeybind;

["missionStarted", {
    if (profileNamespace getVariable [QGVAR(portOverLoadouts), true]) then {
        call FUNC(callPortLoadouts);
    };
}] call CFUNC(addEventhandler);
