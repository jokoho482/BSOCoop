#include "macros.hpp"
/*
    BSO Coop

    Author: Lawrence

    Description:
    initilize increased speed

    Parameter(s):
    None

    Returns:
    None
*/

GVAR(speedModifier) = [CFGCOMMON(speedModifier), 1] call CFUNC(getSetting);
GVAR(infiniteStamina) = !(([CFGCOMMON(infiniteStamina), 1] call CFUNC(getSetting)) == 1);

["playerChanged", {
    CLib_Player setAnimSpeedCoef GVAR(speedModifier);
    CLib_Player enableStamina GVAR(infiniteStamina);
}] call CFUNC(addEventhandler);
