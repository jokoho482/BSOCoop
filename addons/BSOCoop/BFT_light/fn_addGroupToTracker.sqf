#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas, BadGuy

    Description:
    Add Group Icon to a Group

    Parameter(s):
    None

    Returns:
    None
*/
params ["_leader", "_iconID", ["_attachTo", [0, -20]]];

private _defaultIcon = GETSIDE(_leader) call FUNC(getDefaultIcon);

private _icon = (group _leader) getVariable [QGVAR(icon), _defaultIcon];

private _groupName = groupId (group _leader);
private _data = GVAR(iconNamespace) getVariable _icon;
_leader = [objectParent _leader, _leader] select (isNull objectParent _leader);
private _iconPos = [_leader, _attachTo];
DUMP("AddMapGraphics");
_data params ["_groupMapIcon", "_color", "_size"];
[
    _iconID,
    [
        ["ICON", _groupMapIcon, _color, _iconPos, _size, _size],
        ["ICON", "a3\ui_f\data\Map\Markers\System\dummy_ca.paa", [1, 1, 1, 1], _iconPos, 25, 25, 0, _groupName, 2]
    ]
] call CFUNC(addMapGraphicsGroup);

[_iconID, "hoverin", {
    params ["_posData","_offSetData"];
    _posData params ["_map", "_xPos", "_yPos"];
    _offSetData params ["_unit", "_attachTo"];
    _attachTo params ["_iconPos", "_offSet"];
    with uiNamespace do {
        GVAR(ttRadio) ctrlSetText format [
            "SR: %1 | LR: %2",
            (group _unit) getVariable [QGVAR(radioSR), "n/a"],
            (group _unit) getVariable [QGVAR(radioLR), "n/a"]
        ];
        GVAR(ttRadio) ctrlCommit 0;
        GVAR(ttRemarks) ctrlSetText ((group _unit) getVariable [QGVAR(remarks),""]);
        GVAR(ttRemarks) ctrlCommit 0;
        private _pos = _map ctrlMapWorldToScreen getPosVisual (_iconPos);
        _pos set [0, (_pos select 0) + 15 / 640];
        _pos set [1, (_pos select 1) + (((_offSet) select 1) + 5) / 480];

        GVAR(groupToolTip) ctrlSetPosition _pos;
        GVAR(groupToolTip) ctrlShow true;
        GVAR(groupToolTip) ctrlCommit 0;
    };
}, [_leader, _iconPos]] call CFUNC(addMapGraphicsEventHandler);

[_iconID, "hoverout", {
    with uiNamespace do {
        GVAR(groupToolTip) ctrlShow false;
        GVAR(groupToolTip) ctrlCommit 0;
    };
}] call CFUNC(addMapGraphicsEventHandler);

[_iconID, "clicked", {
    (_this select 1) params ["_unit"];
    call FUNC(onHoverOut);
    if !((group _unit) isEqualTo (group player)) then {
        _unit call FUNC(onHoverIn);
    };
}, _leader] call CFUNC(addMapGraphicsEventHandler);
