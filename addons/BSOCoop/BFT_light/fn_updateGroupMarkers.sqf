#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas, BadGuy

    Description:
    Update all Group Marker

    Parameter(s):
    None

    Returns:
    None
*/
RUNTIMESTART;
private _allGroups = [];
{
    [_x] call CFUNC(removeMapGraphicsGroup);
} count (GVAR(allIcons) select 0);

{
    private _leader = leader _x;
    if (_leader call FUNC(isValidUnit)) then {
        if (isNull objectParent _leader) then {
            private _iconId = toLower format [QGVAR(IconId_Group_%1_%2_%3), _x, _leader, _x isEqualTo (group player)];
            _allGroups pushBack _iconId;
            [_leader, _iconId, [0, -20]] call FUNC(addGroupToTracker);
        } else {
            private _vehicle = vehicle _leader;
            private _nbrGroups = 0;
            {
                if (leader _x == _x) then {
                    if !(_x call FUNC(isValidUnit)) exitWith {};
                    _nbrGroups = _nbrGroups + 1;
                    private _iconId = toLower format [QGVAR(IconId_Group_%1_%2_%3_%4), group _x, _vehicle, group _x isEqualTo group player, _nbrGroups];
                    _allGroups pushBack _iconId;
                    [leader _x, _iconId, [0, -20 * _nbrGroups]] call FUNC(addGroupToTracker);
                };
                nil
            } count (crew _vehicle);
        };
    };
    nil
} count allGroups;


GVAR(allIcons) set [0, _allGroups];

RUNTIME("Update Group Icons");
