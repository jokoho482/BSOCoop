#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas, BadGuy

    Description:
    Get Default Icon for Side

    Parameter(s):
    None

    Returns:
    None
*/
params ["_side"];
switch (_side) do {
    case (west): {
        "b_unknown"
    };
    case (east): {
        "o_unknown"
    };
    case (independent): {
        "n_unknown"
    };
    case (civilian): {
        "c_unknown"
    };
    default {
        "hd_unknown"
     };
};
