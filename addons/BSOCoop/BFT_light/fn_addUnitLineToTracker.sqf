#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas, BadGuy

    Description:
    Update all Player Marker

    Parameter(s):
    None

    Returns:
    None
*/

if !(GVAR(drawLinesClient)) exitWith {};
params ["_iconID", "_unit", "_target"];
if (_target == _unit) exitWith {};

private _team = _unit getVariable [QGVAR(assignedTeam), "MAIN"];
private _color = if (leader _target == _unit) then {
    [1, 0.4, 0, 1];
} else {
    [
        [0.62, 0.7, 0.94, 1],
        [1, 0, 0.1, 1],
        [0.1, 1, 0, 1],
        [0.1, 0, 1, 1],
        [1, 1, 0.1, 1]
    ] select ((["MAIN", "RED", "GREEN", "BLUE", "YELLOW"] find ([_team] param [0, "MAIN"])) max 0);
};

[
    _iconID,
    [
        ["LINE", _target, _unit, _color]
    ]
] call CFUNC(addMapGraphicsGroup);
