#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas, BadGuy

    Description:
    Update all Player Marker

    Parameter(s):
    None

    Returns:
    None
*/
with missionNamespace do {
    {
        [_x] call CFUNC(removeMapGraphicsGroup);
        [_x + "line"] call CFUNC(removeMapGraphicsGroup);
    } count (GVAR(allIcons) select 2);
    GVAR(allIcons) set [2, []];
    DUMP("Remove External Group Icons")
};
