#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas, BadGuy

    Description:
    Update all Player Marker

    Parameter(s):
    None

    Returns:
    None
*/
RUNTIMESTART;
{
    [_x] call CFUNC(removeMapGraphicsGroup);
    [_x + "line"] call CFUNC(removeMapGraphicsGroup);
    nil
} count (GVAR(allIcons) select 1);
private _allPlayers = [];
if !(GVAR(showGroupPlayer)) exitWith {};
{
    if (_x call FUNC(isValidUnit)) then {
        if (isNull objectParent _x) then {
            private _iconID = format [QGVAR(unit_%1_%2), _x, group _x];
            [_iconID, _x] call FUNC(addUnitToTracker);
            _allPlayers pushBack _iconID;
        } else {
            private _vehicle = objectParent _x;
            private _iconID = format [QGVAR(vehicle_%1), _vehicle];
            [_iconID, _vehicle] call FUNC(addVehicleToTracker);
            _allPlayers pushBack _iconID;
        };
    };
    nil
} count (units player);


GVAR(allIcons) set [1, _allPlayers];
RUNTIME("Update Player Icons");
