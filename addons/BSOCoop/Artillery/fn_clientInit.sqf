#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:


    Parameter(s):
    None

    Returns:
    None
*/
[QGVAR(firedNotification), {
    (_this select 0) params ["_eta", "_type", "_rounds", "_pos"];
    hint format ["We Fired %3 rounds of %2 ETA in %1 at %4", _eta, _type, _rounds, _pos];
}] call CFUNC(addEventhandler);
[QGVAR(cantHitTarget), {
    hint "Cant Hit Target!";
}] call CFUNC(addEventhandler);

GVAR(usedItems) = ([CFGARTILLERY(usedItems), ["Binocular"]] call CFUNC(getSetting)) apply {toLower _x};

GVAR(ArtilleryUnits) = ([CFGARTILLERY(usedItems), ["Binocular"]] call CFUNC(getSetting));
GVAR(ArtilleryUnits) = GVAR(ArtilleryUnits) apply {missionNamespace getVariable [_x, objNull]};
GVAR(ArtilleryUnits) = GVAR(ArtilleryUnits) - [objNull];

GVAR(isLasering) = false;
GVAR(testTarget) = "Sign_Arrow_F" createVehicle (getPos CLib_Player);
["", CLib_Player, 0, {
    (toLower (currentWeapon CLib_Player) in GVAR(usedItems)) && GVAR(isLasering) && cameraView == "GUNNER"
}, {
    hint "Fire Ari Test";
    private _ins = lineIntersectsSurfaces [AGLToASL positionCameraToWorld [0,0,0], AGLToASL positionCameraToWorld [0, 0, 10000], player, objNull, true, 1, "VIEW", "FIRE",true];
    if (count _ins == 0) exitWith {GVAR(testTarget) setPosASL (getPos CLib_Player);};
    GVAR(testTarget) setPosASL ((_ins select 0) select 0);
    GVAR(testTarget) setVectorUp ((_ins select 0) select 1);
}, ["priority", 0,"showWindow", false,"shortcut", "DefaultAction"]] call CFUNC(addAction);

["Request Artillery", CLib_Player, 0, {
    (toLower (currentWeapon CLib_Player) in GVAR(usedItems)) && !GVAR(isLasering)
}, {
    GVAR(isLasering) = true;
    nil
}, ["showWindow", false]] call CFUNC(addAction);

["Cancel Request Artillery", CLib_Player, 0, {
    (toLower (currentWeapon CLib_Player) in GVAR(usedItems)) && GVAR(isLasering)
}, {
    GVAR(isLasering) = false;
    nil
}, ["showWindow", false]] call CFUNC(addAction);
["currentWeaponChanged", {
    GVAR(isLasering) = false;
    nil
}] call CFUNC(addEventhandler);
