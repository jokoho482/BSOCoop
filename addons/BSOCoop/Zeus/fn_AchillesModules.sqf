#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:
    client initilize Zeus FPS System

    Parameter(s):
    None

    Returns:
    None
*/

if (call EFUNC(Common,AchillesLoaded)) then {
    ["BSO Coop", "Don't Allow Deleting", {
        params ["", "_selected_object"];

        private _selected_objects = if (isNull _selected_object) then {
            ["objects"] call Achilles_fnc_SelectUnits;
        } else {
            [_selected_object];
        };
        if (isNil "_selected_objects") exitWith {};
        if (_selected_objects isEqualTo []) exitWith {
            ["No object was selected!"] call Ares_fnc_ShowZeusMessage;
            playSound "FD_Start_F";
        };

        private _count = {
            _x setVariable [QCGVAR(noClean), true, true];
            true
        } count _selected_objects;
        ["%1 Objects are not allowed to get deleted", _count] call Ares_fnc_ShowZeusMessage;
        playSound "FD_Start_F";
        nil
    }] call Ares_fnc_RegisterCustomModule;

    ["BSO Coop", "Toggle Zeus FPS", {
        call FUNC(ToggleZeusFPS);
        nil
    }] call Ares_fnc_RegisterCustomModule;

    ["BSO Coop", "Allow Deleting", {
        params ["", "_selected_object"];

        private _selected_objects = if (isNull _selected_object) then {
            ["objects"] call Achilles_fnc_SelectUnits;
        } else {
            [_selected_object];
        };
        if (isNil "_selected_objects") exitWith {};
        if (_selected_objects isEqualTo []) exitWith {
            ["No object was selected!"] call Ares_fnc_ShowZeusMessage;
            playSound "FD_Start_F";
        };

        private _count = {
            _x setVariable [QCGVAR(noClean), false, true];
            true
        } count _selected_objects;

        ["%1 Objects are allowed to get deleted", _count] call Ares_fnc_ShowZeusMessage;
        playSound "FD_Start_F";
        nil
    }] call Ares_fnc_RegisterCustomModule;

    ["BSO Coop", "Check Mission Performance", {
        private _dialog_result = ["Mission Performance Module",
            [
                ["", ["Mission Objects", "Units", "Weapon Holders", "Entities", "Vehicles"], 0]
            ]
        ] call Ares_fnc_showChooseDialog;
        if (_dialog_result isEqualTo []) exitWith {};

        private _result = _dialog_result select 0;

        switch (_result) do {
            case (0): {
                private _count = count allMissionObjects "";
                ["%1 Mission Objects", _count] call Ares_fnc_ShowZeusMessage;
                playSound "FD_Start_F";
            };
            case (1): {
                private _count = count allUnits;
                ["%1 Units", _count] call Ares_fnc_ShowZeusMessage;
                playSound "FD_Start_F";
            };
            case (2): {
                private _objs = (allMissionObjects "WeaponHolder") + (allMissionObjects "GroundWeaponHolder") + (allMissionObjects "WeaponHolderSimulated");
                _objs = _objs arrayIntersect _objs;
                private _count = count _objs;
                ["%1 Weapon Holder", _count] call Ares_fnc_ShowZeusMessage;
                playSound "FD_Start_F";
            };
            case (3): {
                private _count = count (entities [[], [], true, false]);
                ["%1 Entities", _count] call Ares_fnc_ShowZeusMessage;
                playSound "FD_Start_F";
            };
            case (4): {
                private _count = count vehicles;
                ["%1 Vehicles", _count] call Ares_fnc_ShowZeusMessage;
                playSound "FD_Start_F";
            };
            default {
                ["Error No Value was selected"] call Ares_fnc_ShowZeusMessage;
                playSound "FD_Start_F";
            };
        };
    }] call Ares_fnc_RegisterCustomModule;

    ["BSO Coop", "End Mission Screen", {
        private _endTypes = ["continue", "killed","loser","end1","end2","end3","end4","end5","end6"];
        {
            {
                _endTypes pushBackUnique toLower (configName _x);
                nil
            } count configProperties [_x >> "CfgDebriefing", "isClass _x", true];
            nil
        } count [missionConfigFile, configFile, campaignConfigFile];
        private _dialog_result = ["End Mission Screen",
            [
                ["Success?", ["Success","Fail"], 0],
                ["Play Music?", ["Yes","No"], 0],
                ["End Type", _endTypes, 3]
            ]
        ] call Ares_fnc_showChooseDialog;
        if (_dialog_result isEqualTo []) exitWith {};
        _dialog_result params ["_win", "_music", "_endType"];
        ["missionEndRunTime", [_win isEqualTo 0, _music isEqualTo 0, _endTypes select _endType]] call CFUNC(globalEvent);
        nil
    }] call Ares_fnc_RegisterCustomModule;

    ["BSO Coop", "Set Global AI Skill", {

        private _dialog_result = ["Set Global AI Skill",
            [
                ["General", "SLIDER", 0.85],
                ["Commanding", "SLIDER", 0.85],
                ["Courage", "SLIDER", 0.85],
                ["Aiming Accuracy", "SLIDER", 0.85],
                ["Aiming Shake", "SLIDER", 0.85],
                ["Aiming Speed", "SLIDER", 0.85],
                ["Reload Speed", "SLIDER", 0.85],
                ["Spot Distance", "SLIDER", 0.85],
                ["Spot Time", "SLIDER", 0.85],
                ["Use Cover", ["True", "False"], 0],
                ["Use AutoCombat", ["True", "False"], 0],
                ["Update for New Units", ["Enable", "Disable"], 0],
                ["Global?", ["True", "False"], 1]
            ]
        ] call Ares_fnc_showChooseDialog;
        if (_dialog_result isEqualTo []) exitWith {};
        _dialog_result params [
            "_general", "_commanding", "_courage", "_aimAcc",
            "_aimShake", "_amimSpeed", "_reloadSpeed",
            "_spotDis", "_spotTime",
            "_cover", "_combat",
            "_useUpdate", "_global"
        ];

        _cover = (_cover isEqualTo 0);
        _combat = (_combat isEqualTo 0);
        _useUpdate = (_useUpdate isEqualTo 0);
        _global = (_global isEqualTo 0);

        private _data = [
            _general, _commanding, _courage, _aimAcc,
            _aimShake, _amimSpeed, _reloadSpeed,
            _spotDis, _spotTime,
            _cover, _combat
        ];

        if (_useUpdate) then {
            GVAR(autoUpdateSkills) = _data;
            GVAR(setAISkillGlobal) = _global;
        } else {
            GVAR(autoUpdateSkills) = nil;
        };

        {
            if (local _x) then {
                [_x, _data] call FUNC(setGlobalAISkill);
            } else {
                if (_global) then {
                    [_x, _data] call FUNC(setGlobalAISkill);
                };
            };
            nil
        } count allUnits;
        nil
    }] call Ares_fnc_RegisterCustomModule;

    ["BSO Coop", "Add ACE Arsenal", {
        params ["", "_selected_object"];

        private _selected_objects = if (isNull _selected_object) then {
            ["objects"] call Achilles_fnc_SelectUnits;
        } else {
            [_selected_object];
        };
        if (isNil "_selected_objects") exitWith {};
        if (_selected_objects isEqualTo []) exitWith {
            ["No object was selected!"] call Ares_fnc_ShowZeusMessage;
            playSound "FD_Start_F";
        };

        private _count = {
            [_x, true] call ace_arsenal_fnc_initBox;
            true
        } count _selected_objects;
        ["%1 Objects are not allowed to get deleted", _count] call Ares_fnc_ShowZeusMessage;
        playSound "FD_Start_F";
        nil
    }] call Ares_fnc_RegisterCustomModule;

};
