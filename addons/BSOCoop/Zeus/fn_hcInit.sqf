#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:
    initilize Zeus System

    Parameter(s):
    None

    Returns:
    None
*/
[{
    if (GVAR(FrameStatusIsRunning)) then {
        CLib_Player setVariable [QGVAR(currentFPS), diag_fps, true];
    };
}, 0.1] call CFUNC(addPerFrameHandler);

CLib_Player setVariable [QGVAR(currentFPS), diag_fps, true];
