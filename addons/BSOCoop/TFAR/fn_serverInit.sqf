#include "macros.hpp"
/*
    BSO Coop
    Author: joko // Jonas
    Description:
    initilize TFAR Settings System
    Parameter(s):
    None
    Returns:
    None
*/

GVAR(forceTFAR) = (([CFGTFAR(forceTFAR), 1] call CFUNC(getSetting)) isEqualTo 1);
publicVariable QGVAR(forceTFAR);
