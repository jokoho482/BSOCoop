#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:
    initilize ACRE2 Module

    Parameter(s):
    None

    Returns:
    None
*/


if (call EFUNC(Common,AchillesLoaded)) then {
    ["BSO Coop", "Enable ACRE Spectator Sounds", {
        private _dialogResult =
        [
            "ACRE Spectator Sounds",
            [
                [ "Mode", ["Deactivate", "Activate"], 0]
            ]
        ] call Ares_fnc_ShowChooseDialog;
        if (_dialogResult isEqualTo []) exitWith {};
        _dialogResult params ["_value"];
        [_value isEqualTo 1] call acre_api_fnc_setSpectator;
        playSound "ClickSoft";
    }] call Ares_fnc_RegisterCustomModule;
};

#include "\a3\editor_f\Data\Scripts\dikCodes.h"
[
    "BSO Coop",
    "Toggle_ACRE_Spectator",
    "Toggle ACRE Spectator",
    {
        if (isNull curatorCamera) exitWith {};
        [!(player call acre_api_fnc_isSpectator)] call acre_api_fnc_setSpectator;
        playSound "ClickSoft";
        nil
    }, "", [DIK_F7, [false, false, false]]
] call CBA_fnc_addKeybind;

["exitZeus", {
    [false] call acre_api_fnc_setSpectator;
    playSound "ClickSoft";
}] call CFUNC(addEventhandler);

/*
//Setting 152 Radios
["ACRE_PRC152", "default", "BSO_152_preset"] call acre_api_fnc_copyPreset;

["ACRE_PRC152", "BSO_152_preset", 1, "description", "ALPHA NET"] call acre_api_fnc_setPresetChannelField;
["ACRE_PRC152", "BSO_152_preset", 2, "description", "BRAVO NET"] call acre_api_fnc_setPresetChannelField;
["ACRE_PRC152", "BSO_152_preset", 3, "description", "CHARLIE NET"] call acre_api_fnc_setPresetChannelField;
["ACRE_PRC152", "BSO_152_preset", 4, "description", "DELTA NET"] call acre_api_fnc_setPresetChannelField;
["ACRE_PRC152", "BSO_152_preset", 5, "description", "PLT. NET"] call acre_api_fnc_setPresetChannelField;
["ACRE_PRC152", "BSO_152_preset", 6, "description", "AIR NET"] call acre_api_fnc_setPresetChannelField;
["ACRE_PRC152", "BSO_152_preset", 7, "description", "ARMOR NET"] call acre_api_fnc_setPresetChannelField;

// Setting 117 Radios
["ACRE_PRC148", "default", "BSO_148_preset"] call acre_api_fnc_copyPreset;

["ACRE_PRC148", "BSO_148_preset", 1, "label", "ALPHA NET"] call acre_api_fnc_setPresetChannelField;
["ACRE_PRC148", "BSO_148_preset", 2, "label", "BRAVO NET"] call acre_api_fnc_setPresetChannelField;
["ACRE_PRC148", "BSO_148_preset", 3, "label", "CHARLIE NET"] call acre_api_fnc_setPresetChannelField;
["ACRE_PRC148", "BSO_148_preset", 4, "label", "DELTA NET"] call acre_api_fnc_setPresetChannelField;
["ACRE_PRC148", "BSO_148_preset", 5, "label", "PLT. NET"] call acre_api_fnc_setPresetChannelField;
["ACRE_PRC148", "BSO_148_preset", 6, "label", "AIR NET"] call acre_api_fnc_setPresetChannelField;
["ACRE_PRC148", "BSO_148_preset", 7, "label", "ARMOR NET"] call acre_api_fnc_setPresetChannelField;

//Setting 117 Radios
["ACRE_PRC117F", "default", "BSO_117_preset"] call acre_api_fnc_copyPreset;

["ACRE_PRC117F", "BSO_117_preset", 1, "name", "ALPHA NET"] call acre_api_fnc_setPresetChannelField;
["ACRE_PRC117F", "BSO_117_preset", 2, "name", "BRAVO NET"] call acre_api_fnc_setPresetChannelField;
["ACRE_PRC117F", "BSO_117_preset", 3, "name", "CHARLIE NET"] call acre_api_fnc_setPresetChannelField;
["ACRE_PRC117F", "BSO_117_preset", 4, "name", "DELTA NET"] call acre_api_fnc_setPresetChannelField;
["ACRE_PRC117F", "BSO_117_preset", 5, "name", "PLT. NET"] call acre_api_fnc_setPresetChannelField;
["ACRE_PRC117F", "BSO_117_preset", 6, "name", "AIR NET"] call acre_api_fnc_setPresetChannelField;
["ACRE_PRC117F", "BSO_117_preset", 7, "name", "ARMOR NET"] call acre_api_fnc_setPresetChannelField;

GVAR(unitArray) = ["ASL","CSL","BSL","DSL","AM","BM","CM","DM","CO","Sarge","FAC","PltM","A1TL","A2TL","B1TL","B2TL","C1TL","C2TL","D1TL","D2TL","Ph1","Ph11","Ph2","Ph22","Ph3","Ph33"];

DFUNC(replaceRadio) = {
    {
        CLib_player removeItem _x;
        CLib_player addItem (_x call acre_api_fnc_getBaseRadio);
        DUMP("replace Radio");
        true
    } count (call acre_api_fnc_getCurrentRadioList);
};

[{
    ["ACRE_PRC152", "BSO_152_preset"] call acre_api_fnc_setPreset;
    ["ACRE_PRC117F", "BSO_117_preset"] call acre_api_fnc_setPreset;
    ["ACRE_PRC148", "BSO_148_preset"] call acre_api_fnc_setPreset;

    // if ((call FUNC(replaceRadio)) != 0) exitWith {};

    private _strPlayer = str CLib_player;
    if (_strPlayer in GVAR(unitArray)) then {
        CLib_player addItem "ACRE_PRC152";
    };
}, {
    [] call acre_api_fnc_isInitialized;
}] call CFUNC(waitUntil);
*/
