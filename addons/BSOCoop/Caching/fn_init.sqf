#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:

    Parameter(s):
    None

    Returns:
    None
*/
["missionStarted", {
    private _group = [CFGCACHING(DistanceGroup), "2000"] call CFUNC(getSetting);
    private _vehicle = [CFGCACHING(DistanceVehicle), "2500"] call CFUNC(getSetting);
    private _eVehicle = [CFGCACHING(DistanceEmptyVehicle), "1000"] call CFUNC(getSetting);
    private _props = [CFGCACHING(DistanceProps), "500"] call CFUNC(getSetting);
    private _coef = [CFGCACHING(CoefDefault), "2.2"] call CFUNC(getSetting);

    GVAR(defaultValues) = [
        _group,
        _vehicle,
        _eVehicle,
        _props,
        _coef
    ];
}] call CFUNC(addEventhandler);


DFUNC(handleZeusEdit) = {
    params ["_obj"];
    switch (_this) do {
        case "ARRAY": {
            {
                _x call FUNC(handleZeusEdit);
                nil
            } count _obj;
        };
        case "OBJECT": {
            if (_obj isKindOf "CAManBase") then {
                ["enableDynamicSimulation", [group _obj, false]] call CFUNC(serverEvent);
                [{
                    ["enableDynamicSimulation", [_this, true]] call CFUNC(serverEvent);
                }, 1] call CFUNC(wait);
            } else {
                ["enableDynamicSimulation", [_obj, false]] call CFUNC(serverEvent);
                [{
                    ["enableDynamicSimulation", [_this, true]] call CFUNC(serverEvent);
                }, 1] call CFUNC(wait);
                private _crew = crew _obj;
                if !(_crew isEqualTo []) then {
                    _crew call FUNC(handleZeusEdit);
                };
                private _units = units _obj;
                if !(_units isEqualTo []) then {
                    _units call FUNC(handleZeusEdit);
                    (group _obj) call FUNC(handleZeusEdit);
                };
            };
        };
        case "GROUP": {
            ["enableDynamicSimulation", [_obj, false]] call CFUNC(serverEvent);
            [{
                ["enableDynamicSimulation", [_this, true]] call CFUNC(serverEvent);
            }, 1, _obj] call CFUNC(wait);
        };
    };
};

["entityCreated", {
    (_this select 0) params ["_obj"];
    if (_obj isKindOf "ModuleCurator_F") then {
        _obj addEventHandler ["CuratorObjectEdited", {
            params ["", "_object"];
            _object call FUNC(handleZeusEdit);
            (group _object) call FUNC(handleZeusEdit);
        }];
    };
}] call CFUNC(addEventhandler);

// TODO: find a Way to Handle the Zeus Move.
{
    _x addEventHandler ["CuratorObjectEdited", {
        params ["", "_object"];
        _object call FUNC(handleZeusEdit);
        (group _object) call FUNC(handleZeusEdit);
    }];
    nil
} count allCurators;
