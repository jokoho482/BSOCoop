#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas & Lawrence

    Description:
    initilize ScoreBoard Fix Module

    Parameter(s):
    None

    Returns:
    None
*/

["missionStarted", {
    private _enableFix = ([CFGSCOREBOARD(enableFix), 1] call CFUNC(getSetting)) == 1;
    if !(_enableFix) exitWith {DUMP("BSO scoreboard disabled. PVP mode on.")};
    DUMP("BSO scoreboard enabled. PVP mode off.");
    call FUNC(killMonitor);
}, []] call CFUNC(addEventHandler);
