#include "macros.hpp"
/*
    BSO Coop

    Author: joko // Jonas

    Description:
    initilize ACE CPR Module

    Parameter(s):
    None

    Returns:
    None
*/

["ace_medical_treatmentAdvanced_CPRLocal", {
    params ["", "_target"];
    if (_target getVariable ["ace_medical_inReviveState", false]) then {
        private _reviveStartTime = _target getVariable ["ace_medical_reviveStartTime", 0];
        if (_reviveStartTime > 0) then {
            _target setVariable ["ace_medical_reviveStartTime", (_reviveStartTime + random [15,20,30]) min CBA_missionTime];
        };
    };
    if !(_target call ace_medical_fnc_isInStableCondition) exitWith {};
    if !(_target getVariable ["ace_isUnconscious", false]) exitWith {};

    private _epiStatus = (_target getVariable ["ace_medical_epinephrine_insystem", 0]) min 1;
    private _bloodVolume = (_target getVariable ["ace_medical_bloodVolume", 100]) / 100;

    if (_epiStatus < 0.2) exitWith {};

    private _reqValue = (_epiStatus + _bloodVolume) / 3;

    if ((random 1) >= _reqValue) then {
        _target setVariable ["ace_medical_inCardiacArrest", nil,true];
        _target setVariable ["ace_medical_heartRate", 40];
        _target setVariable ["ace_medical_bloodPressure", [50,70]];
        [_target, false] call ace_medical_fnc_setUnconscious;
    };
}] call CBA_fnc_addEventHandler;
