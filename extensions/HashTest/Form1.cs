﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HashTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = GenerateHash(textBox1.Text);
        }

        public string GenerateHash(string input)
        {
            int output = 0;
            for (int i = 0; i != input.Length; i++)
            {
                output += Char.ConvertToUtf32(input, (int)i) % (2^32);
            }
            label2.Text = output.ToString();
            return "0x" + output.ToString("x").ToUpper();
        }


    }
}

